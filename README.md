This is repository with Jenkins pipeline configuration.

## Description

This repository will be used by Jenkins seed job to create scheduled pipeline.

Created pipeline will download latest wt-agent and install it in JENKINS-HOME repository.

Required job param is AGENT_TYPE (e.g. jira, invoice-demo etc). It will be set by seed job. If you need to import with job manually be sure to set it.

Optional env variable is STREAM (default value: release). You can set STREAM=beta to get latest staging agent.

Optional env variable is VERSION (default value: latest). You can set VERSION=3.0.1-211 to get the speicific agent version.
